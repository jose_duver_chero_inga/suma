package com.suma;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

;
@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button suma = (Button)findViewById(R.id.btnSumar);
		suma.setOnClickListener(OkListener);
	
	}


	private OnClickListener OkListener = new OnClickListener(){
		
		public void onClick(View view){
			calcularSuma();
		}
		
	};
	

	public void calcularSuma(){
		
		EditText numero1 = (EditText)findViewById(R.id.etNumero1);
		EditText numero2 = (EditText)findViewById(R.id.etNumero2);
		int intNumero1 = Integer.parseInt(numero1.getText().toString());
		int intNumero2 = Integer.parseInt(numero2.getText().toString());
		int totalSuma = intNumero1 + intNumero2;
		Toast.makeText(this, "El resultado de la suma es: " + totalSuma,Toast.LENGTH_LONG).show();
	}

}